$(document).on('turbolinks:load', function() {
    
    $(".blockwidget").draggable({ helper: 'clone', cursor: "move"});
    $(".blockelement").draggable({ helper: 'clone', cursor: "move"});

    $('#cartwidget').droppable({
        accept: '.blockwidget',
        activeClass: 'active',
        hoverClass: 'hover',
        drop: function(ev, ui) {
            var droppeditem = $(ui.draggable).clone();
            droppeditem.find('.img-element').remove();
            droppeditem.find('.d-none').removeClass('d-none');
            droppeditem.find('.row').append($('<button type="button" class="btn btn-default btn-xs remove"><span class="glyphicon glyphicon-trash"></span></button>'));
            droppeditem.find('.remove').click(function () { 
                console.log("delete");
                $(this).parent().parent().detach() 
            });
            $(this).append(droppeditem);

            console.log("widget");

            $('.element').droppable({
                accept: '.blockelement',
                drop: function(ev, ui) {
                    console.log("element");
                    console.log('test', $(this));
                    var item = $(ui.draggable).clone();
                    item.find('.img-element').remove();
                    item.find('.d-none').removeClass('d-none');
                    droppeditem.find('.split').toggleClass('split element-split');
                    droppeditem.find('.my-container-left').toggleClass('my-container-left element-my-container-left');
                    console.log("item14", item);
                    // item.find('.content').append($('<button type="button" class="btn btn-default btn-xs remove-element"><span class="glyphicon glyphicon-trash"></span></button>'));
                    // $('.remove-element').click(function () {
                    //     console.log("delete111"); 
                    //     $(this).parent().detach() 
                    // });
                    if(item.attr('editor') == undefined) {
                        console.log("editable");
                        item.attr("contenteditable", "true");
                        CKEDITOR.inline( item.get( 0 ) );
                    }
                    $(this).append(item);
                }
            })
        }
    });
    

    $('.save-btn').click(function() {
        item = $(this).parent().parent().parent();
        item.find('.layout').remove();
        item.find('.element-split').removeClass('element-split');
        item.find('.element-my-container-left').removeClass('element-my-container-left');
        item.find('.remove').remove();
        item.find('.remove-element').remove();
        item.find('.blockelement').attr("contenteditable", "false");
        item.find('.dynamic').each(function(index, element){
            index = parseInt($(element).data('index'));
            console.log("index", index);
            $.ajax({
                type:"get",
                url:`/content/${index}`,
                async: false,
                beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
                success:function(result){
                    $(element).text(result.body);
                }
            });
        });
        item111 = item[0].outerHTML.replace(/\&lt;/g, '<').replace(/\&gt;/g, '>');
        $.ajax({
			type: "post",
            url: "/design",
            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
			data: { body: item111 },
		})
    })

    $('.save-preview-btn').click(function() {
        item = $(this).parent().parent().parent();
        item.find('.layout').remove();
        item.find('.element-split').removeClass('element-split');
        item.find('.element-my-container-left').removeClass('element-my-container-left');
        item.find('.remove').remove();
        item.find('.remove-element').remove();
        var item = item[0].outerHTML;
        $.ajax({
			type: "post",
            url: "/preview",
            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
			data: { body: item },
		})
    })
})
