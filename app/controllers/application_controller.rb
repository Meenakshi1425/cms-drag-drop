class ApplicationController < ActionController::Base
    before_action :widget, :design

    private
    def widget
        if (@widget = Widget.find_by_published(true))
            @header = Header.find(@widget.header_id)
            @menus = @header.menus
            @logo = @header.logo
        end
    end

    def design
        @design = Design.last
        @active = @design['active']
    end
end
