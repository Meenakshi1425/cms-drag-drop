class BannerController < ApplicationController
    def edit
        @banner = Banner.last
    end

    def update
        @banner = Banner.last
        if @banner.update_attributes(set_params)
            redirect_to cms_path
        else
            render :edit
        end
    end

    private
    def set_params
        params.require(:banner).permit(:image, :body)
    end
end
