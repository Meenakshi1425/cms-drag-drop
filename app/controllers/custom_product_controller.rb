class CustomProductController < ApplicationController
    def edit
        @custom_product = CustomProduct.last
    end

    def update
        @custom_product = CustomProduct.last
        params[:custom_product][:no_of_products] =  params[:custom_product][:no_of_products].to_i if params[:custom_product][:no_of_products].present?
        if @custom_product.update_attributes(set_product)
            redirect_to cms_path
        else
            render :edit
        end
    end

    private
    def set_product
        params.require(:custom_product).permit(:sort_field, :sort_direct, :no_of_products)
    end
end

