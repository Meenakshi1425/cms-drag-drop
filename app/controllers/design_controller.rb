class DesignController < ApplicationController
  def index
    @design = Design.last.body
  end

  def create
    @design = Design.new({ body: params['body'] })
    redirect_to cms_path if @design.save
  end

  def update
    @design = Design.find(params[:id])
    if @design.update_attributes({ active: true})
      redirect_to spree_path
    else
      redirect_to cms_path
    end
  end
end
