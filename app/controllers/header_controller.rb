class HeaderController < ApplicationController
    def edit
    @header = Header.find(params[:id])
    @header.menus.build
  end

  def update
    @header = Header.find(params[:id])
    if @header.update_attributes(set_header_params)
      redirect_to cms_path
    else
      render :edit
    end
  end

  private
  def set_header_params
    params.require(:header).permit(:logo, menus_attributes: Menu.attribute_names.map(&:to_sym).push(:_destroy))
  end
end
