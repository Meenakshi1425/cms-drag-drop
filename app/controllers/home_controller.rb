class HomeController < ApplicationController
  def index
    @design = Design.last
    @widget = Widget.first
    @banner = Banner.last
    @custom_product = CustomProduct.last
    @products = Spree::Product.order("#{@custom_product[:sort_field]} #{@custom_product[:sort_direct]}").limit(@custom_product[:no_of_products])
    @content = Content.all.map do |cont|
      ERB.new(cont.body)
    end
    @header = Header.last
    @logo = @header.logo
    @menus = @header.menus
  end

  def change_site
    Widget.all.each do |w|
      w.published = false
      w.save
    end
    @design = Design.last
    @design.active = false
    @design.save
    redirect_to spree_path
  end
end
