class PreviewController < ApplicationController
    def index
        @preview = Preview.last.body
    end
    
    def create
        @preview = Preview.new({ body: params['body'] })
    
        redirect_to preview_index_path if @preview.save
    end
end
