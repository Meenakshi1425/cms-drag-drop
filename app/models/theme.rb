class Theme < ApplicationRecord
    belongs_to :header  
    has_many :themes
end