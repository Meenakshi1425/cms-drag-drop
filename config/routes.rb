Rails.application.routes.draw do
  get 'content/:id', to: "content#show"
  resources :design
  resources :preview
  mount Ckeditor::Engine => '/ckeditor'
  get 'sample/index'
  get 'default_site', to: 'home#change_site'
  get 'cms', to: "home#index"
  resources :header, only: [:edit, :update]
  resources :banner, only: [:edit, :update]
  resources :custom_product, only: [:edit, :update]

  # This line mounts Spree's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to
  # Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the
  # :at option to something different.
  #
  # We ask that you don't use the :as option here, as Spree relies on it being
  # the default of "spree".
  mount Spree::Core::Engine, at: '/'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
