class AddHeaderToMenus < ActiveRecord::Migration[5.2]
  def change
    add_reference :menus, :header, foreign_key: true
  end
end
