class AddBodyToTheme < ActiveRecord::Migration[5.2]
  def change
    add_column :themes, :body, :string
  end
end
