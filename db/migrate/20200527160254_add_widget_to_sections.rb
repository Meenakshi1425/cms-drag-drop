class AddWidgetToSections < ActiveRecord::Migration[5.2]
  def change
    add_reference :sections, :widget, foreign_key: true
  end
end
