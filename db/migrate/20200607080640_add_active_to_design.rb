class AddActiveToDesign < ActiveRecord::Migration[5.2]
  def change
    add_column :designs, :active, :boolean
  end
end
