class AddDetailToWidgets < ActiveRecord::Migration[5.2]
  def change
    add_column :widgets, :detail, :text
  end
end
