class AddBannerToWidgets < ActiveRecord::Migration[5.2]
  def change
    add_column :widgets, :banner, :text
  end
end
